
#!/usr/bin/env pwsh

Set-StrictMode -Version latest
$ErrorActionPreference = "Stop"

# Get component data and set necessary variables
$component = Get-Content -Path "component.json" | ConvertFrom-Json

# Build 
docker build -f docker/Dockerfile -t gitlab-runner:ubuntu-16.04 .

if ($LastExitCode -ne 0) {
    Write-Error "Can't build GitLab runner container."
}

Write-Host "GitLab runner container successfully build." -ForegroundColor Green
