#!/usr/bin/env pwsh

Set-StrictMode -Version latest
$ErrorActionPreference = "Stop"

# Get component data and set necessary variables
$component = Get-Content -Path "component.json" | ConvertFrom-Json

# Run gitlab runner
$null = docker run -d --name gitlab-runner `
    --restart always `
    -e GITLAB_URL="$($component.gitlab_url)" `
    -e GITLAB_REGISTRATION_TOKEN="$($component.gitlab_registration_token)" `
    -e GITLAB_RUNNER_DESCRIPTION="$($component.gitlab_runner_description)" `
    -e GITLAB_RUNNER_LABELS="$($component.gitlab_runner_labels)" `
    -e GITLAB_RUNNER_RUN_UNTAGGED="$($component.gitlab_runner_run_untagged)" `
    -e GITLAB_RUNNER_LOCKED="$($component.gitlab_runner_locked)" `
    -e GITLAB_RUNNER_ACCESS_LEVEL="$($component.gitlab_runner_access_level)" `
    -v /var/run/docker.sock:/var/run/docker.sock `
    -v $(which docker):$(which docker) `
    gitlab-runner:ubuntu-16.04

if ($LastExitCode -ne 0) {
    Write-Error "Can't run GitLab runner container"
}

Write-Host "GitLab runner container successfully run." -ForegroundColor Green
